
let send = document.getElementById("send");

send.addEventListener('click', () => {
  let name = document.getElementById("name").value;
  let last_name = document.getElementById("lastname").value;

  if (name !== ""){
    if(last_name !== "") {
      axios.post('https://valid-proof.herokuapp.com/api/user/', {
        name,
        last_name
      })
        .then(function (response) {
          console.log(response);
          this.clearFileds();
          this.showToast('¡Usuario registado con exito!')
        })
        .catch(function (error) {
          console.log(error);
        });
    }else {
      this.showToast('¡Los campos no pueden estar vacios!');
    }
  } else {
    this.showToast('¡Los campos no pueden estar vacios!');
  }
});

function showToast(text) {
  let textToast = document.getElementById('toastText').childNodes[0];
  let newText = document.createTextNode(text);
  document.getElementById('toastText').replaceChild(newText, textToast);
  $('.toast').toast('show');
}

function clearFileds(){
  document.getElementById("name").value = "";
  last_name = document.getElementById("lastname").value = "";
}


