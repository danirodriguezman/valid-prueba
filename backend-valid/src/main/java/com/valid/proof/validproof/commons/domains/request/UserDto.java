package com.valid.proof.validproof.commons.domains.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {

    private Integer id;
    private String name;
    private String last_name;
    private Boolean indicted;
}
