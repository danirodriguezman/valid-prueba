package com.valid.proof.validproof.service.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.valid.proof.validproof.commons.domains.request.UserDto;
import com.valid.proof.validproof.model.entity.User;
import com.valid.proof.validproof.repository.user.impl.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;
    private ObjectMapper mapper;

    @Autowired
    public UserService(UserRepository userRepository, ObjectMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    @Override
    public Optional<User> create(UserDto userDto) {
        userDto.setIndicted(Boolean.FALSE);
        return userRepository.create(mapper.convertValue(userDto, User.class));
    }

    @Override
    public Optional<List<User>> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<Iterable<User>> process(List<UserDto> userDtos) {
        return userRepository.process(Arrays.asList(mapper.convertValue(userDtos, User[].class))) ;
    }

    public void deleteAll(){
        userRepository.deleteAll();
    }
}
