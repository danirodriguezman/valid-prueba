package com.valid.proof.validproof.commons.domains.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
public class BaseResponse<T> implements Serializable {

    private T response;
    private HttpStatus status;
    private String timeResponse;
    private String message;

    public BaseResponse(T response, HttpStatus status, String timeResponse, String message) {
        this.response = response;
        this.status = status;
        this.timeResponse = timeResponse;
        this.message = message;
    }
}
