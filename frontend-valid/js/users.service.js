
function getUsers() {
  axios.get('http://valid-proof.herokuapp.com/api/user/')
    .then(res => {
      let tbody = document.createElement("tbody");
      if (res.status !== 204) {
        let data = res.data.response;
        for (let i = 0; i < data.length; i++) {
          let tr = document.createElement('tr');
          Object.assign(data[i], {"process": ""});
          for (let j in data[i]) {
            let td = document.createElement('td');
            if (j === 'indicted') {
              if (data[i][j] === false){
                data[i][j] = 'No';
              }else {
                data[i][j] = 'Si'
              }
            }
            if(j === 'process'){
              let check = document.createElement('input');
              check.type = "checkbox";
              check.id = "checked";
              td.append(check);
            } else {
              td.innerHTML = data[i][j];
            }
            tr.appendChild(td);
          }
          tbody.appendChild(tr);
        }
      } else {
        let tr = document.createElement('tr');
        let td = document.createElement('td');
        td.colSpan = "4";
        td.innerHTML = "No hay usuarios registrados";
        tr.appendChild(td);
        tbody.appendChild(tr);
      }
      document.getElementById('table').appendChild(tbody);

    })
    .catch(err => {
      console.log(err);
    })
}

//Actualizar

let send = document.getElementById("process");

send.addEventListener('click', () => {
  let table = document.getElementById('table');
  let rowLength = table.rows.length;
  let users = [];
  for (let i = 0; i < rowLength; i++) {
    let row = table.rows.item(i);
    let cell = table.rows.item(i).cells;
    let cellLength = cell.length;
    let element = {};
    for (let j = 0; j < cellLength; j++) {
      if (i >= 1) {
        let cellVal = cell.item(j);
        if(j === 4){
          if (cellVal.firstChild.checked === true){
            for (let k = 0; k < 4; k++) {
              switch (k) {
                case 0 : {
                  Object.assign(element, {"id": row.cells[k].innerHTML});
                }
                case 1 : {
                  Object.assign(element, {"name": row.cells[k].innerHTML});
                }
                case 2 : {
                  Object.assign(element, {"last_name": row.cells[k].innerHTML});
                }
                case 3 : {
                  Object.assign(element, {"indicted": row.cells[k].innerHTML === 'No'});
                }
              }
            }
            users.push(element);
          }
        }
      }
    }
  }

  axios.put('https://valid-proof.herokuapp.com/api/user/', users)
    .then(function (response) {
      console.log(response);
      window.location.href = '../pages/indicted.html'
    })
    .catch(function (error) {
      console.log(error);
    });
});
