package com.valid.proof.validproof.repository.user;

import com.valid.proof.validproof.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
    public interface IUserRepository extends JpaRepository<User, Integer> {}
