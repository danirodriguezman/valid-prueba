package com.valid.proof.validproof.web.api.rest.user;

import com.valid.proof.validproof.commons.constants.api.user.EndpointUser;
import com.valid.proof.validproof.commons.domains.request.UserDto;
import com.valid.proof.validproof.commons.domains.response.builder.ResponseBuilder;
import com.valid.proof.validproof.model.entity.User;
import com.valid.proof.validproof.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(EndpointUser.API_USER)
public class UserApi {

    @Autowired
    private UserService userService;

    @PostMapping(EndpointUser.CREATE)
    @ResponseBody
    public ResponseEntity create(@RequestBody UserDto userDto){
        Optional<User> optUser = userService.create(userDto);
        return ResponseBuilder.newBuilder()
                .withBody(optUser.isPresent() ? optUser.get() : new User())
                .withStatus(optUser.isPresent() ? HttpStatus.OK : HttpStatus.CONFLICT)
                .withMessage(optUser.isPresent() ? "User has been created successfully" : "Conflict Error")
                .buildResposne();
    }

    @GetMapping(EndpointUser.FIND_ALL)
    @ResponseBody
    public ResponseEntity findAll() {
        Optional<List<User>> optUsers = userService.findAll();
        return ResponseBuilder.newBuilder()
                .withBody(optUsers.get())
                .withStatus(optUsers.get().size() > 0 ? HttpStatus.OK : HttpStatus.NO_CONTENT)
                .withMessage("List of registered users")
                .buildResposne();
    }

    @PutMapping(EndpointUser.PROCESS)
    @ResponseBody
    public ResponseEntity process(@RequestBody List<UserDto> processDto) {
        Optional<Iterable<User>> optUsers = userService.process(processDto);
        return ResponseBuilder.newBuilder()
                .withBody(optUsers.get())
                .withStatus(HttpStatus.OK)
                .withMessage("User has been updated successfully")
                .buildResposne();
    }

    @DeleteMapping(EndpointUser.DELETE_ALL)
    public ResponseEntity deleteAll() {
        userService.deleteAll();
        return ResponseBuilder.newBuilder()
                .withMessage("All users have been deleted")
                .withStatus(HttpStatus.OK)
                .buildResposne();
    }
}
