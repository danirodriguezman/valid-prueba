package com.valid.proof.validproof;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidProofApplication {

    public static void main(String[] args) {
        SpringApplication.run(ValidProofApplication.class, args);
    }

}
