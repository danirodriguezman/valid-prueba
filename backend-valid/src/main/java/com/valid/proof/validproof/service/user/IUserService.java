package com.valid.proof.validproof.service.user;

import com.valid.proof.validproof.commons.domains.request.UserDto;
import com.valid.proof.validproof.model.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    Optional<User> create(UserDto userDto);

    Optional<List<User>> findAll();

    Optional<Iterable<User>> process(List<UserDto> userDtos);
}
