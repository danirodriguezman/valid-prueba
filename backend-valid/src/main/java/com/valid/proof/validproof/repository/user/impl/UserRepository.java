package com.valid.proof.validproof.repository.user.impl;

import com.valid.proof.validproof.model.entity.User;
import com.valid.proof.validproof.repository.user.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserRepository implements UserRepositoryFacade {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public Optional<User> create(User user) {
        return Optional.of(userRepository.save(user));
    }

    @Override
    public Optional<List<User>> findAll() {
        return Optional.of((List<User>) userRepository.findAll());
    }

    @Override
    public Optional<Iterable<User>> process(Iterable<User> users) {
        return Optional.of(userRepository.saveAll(users));
    }

    public void deleteAll(){
        userRepository.deleteAll();
    }
}
