package com.valid.proof.validproof.commons.constants.api.user;

public interface EndpointUser {

    String API_USER = "/api/user";
    String CREATE = "/";
    String FIND_ALL = "/";
    String PROCESS = "/";
    String DELETE_ALL = "/";
}
