package com.valid.proof.validproof.commons.domains.response.builder;

import com.valid.proof.validproof.commons.domains.response.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class ResponseBuilder<T> {

    private T response;
    private HttpStatus httpStatus;
    private String timeResponse;
    private String message;

    public static ResponseBuilder newBuilder() {
        return new ResponseBuilder();
    }

    public ResponseBuilder withBody(T response){
        this.response = response;
        this.timeResponse = LocalDateTime.now().toString();
        return this;
    }

    public ResponseBuilder withStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        this.timeResponse = LocalDateTime.now().toString();
        return this;
    }

    public ResponseBuilder withMessage(String message) {
        this.message = message;
        this.timeResponse = LocalDateTime.now().toString();
        return this;
    }

    public ResponseEntity buildResposne(){
        BaseResponse base = new BaseResponse(response, httpStatus, timeResponse, message);
        return new ResponseEntity(base, httpStatus);
    }

}
