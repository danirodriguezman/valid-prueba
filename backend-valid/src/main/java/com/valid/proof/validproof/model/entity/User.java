package com.valid.proof.validproof.model.entity;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Daniela.Rodriguez
 */

@Data
@Entity
@Table(name = "user")
@XmlRootElement
public class User implements Serializable {

    @Nullable
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String last_name;
    @Column(name = "indicted")
    private Boolean indicted = false;

}

