package com.valid.proof.validproof.repository.user.impl;

import com.valid.proof.validproof.model.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepositoryFacade {

    Optional<User> create(User user);

    Optional<List<User>> findAll();

    Optional<Iterable<User>> process(Iterable<User> users);

}
